import {
	ripemd160,
	sha256,
} from '@bitauth/libauth';

/**
* Crypto Module.
*/
export class Crypto
{
	/**
	 * SHA256 hash the given input
	 *
	 * @param input {Uint16Array} The input to hash.
	 *
	 * @returns {Uint8Array} The SHA256 Hash of the given input.
	 */
	public static sha256(input: Uint8Array): Uint8Array
	{
		return sha256.hash(input);
	}

	/**
	 * RIPEMD160 hash the given input
	 *
	 * @param input {Uint16Array} The input to hash.
	 *
	 * @returns {Uint8Array} The RIPEMD160 Hash of the given input.
	 */
	public static ripemd160(input: Uint8Array): Uint8Array
	{
		return ripemd160.hash(input);
	}

	/**
	 * Double SHA256 hash the given input
	 *
	 * @param input {Uint16Array} The input to hash.
	 *
	 * @returns {Uint8Array} The Double SHA256 Hash of the given input.
	 */
	public static hash256(input: Uint8Array): Uint8Array
	{
		return sha256.hash(sha256.hash(input));
	}

	/**
	 * RIPEMD160 and then SHA256 hash the given input
	 *
	 * @param input {Uint16Array} The input to hash.
	 *
	 * @returns {Uint8Array} The RIPEMD160+SHA256 Hash of the given input.
	 */
	public static hash160(input: Uint8Array): Uint8Array
	{
		return ripemd160.hash(sha256.hash(input));
	}
}
