import {
	Base58AddressFormatVersion,
	binToHex,
	CashAddressNetworkPrefix,
	CashAddressType,
	CashAddressVersionByte,
	decodeCashAddressFormatWithoutPrefix,
	decodeBase58Address,
	decodeCashAddress,
	encodeBase58AddressFormat,
	encodeCashAddress
} from '@bitauth/libauth'

export interface DecodedAddress
{
	network: string;
	hash: Uint8Array;
	format: string;
	type: string;
}

/**
* Address Entity.
*
* TODO: Rewrite this entire class. It's a mess.
*/
export class Address {
	private _hash: Uint8Array = new Uint8Array()
	private _network: string = ''
	private _type: string = ''

	constructor(hash: Uint8Array, network: string = CashAddressNetworkPrefix.mainnet) {
		this._hash = hash
		this._network = network

		return this
	}

	public toString(): string {
		return this.toCashAddr()
	}

	/**
	 * Converts this address to Legacy Address format.
	 *
	 * @returns {string} The address in Legacy Address format.
	 */
	public toLegacy(): string {
		return encodeBase58AddressFormat(Base58AddressFormatVersion.p2pkh, this._hash);
	}

	/**
	 * Converts this address to CashAddr format.
	 *
	 * @param network {CashAddressNetworkPrefix} The Cash Address Network prefix.
	 * @param type    {CashAddressType}          The type of the address (P2PKH, P2SH, etc).
	 *
	 * @returns {string} The address in CashAddr format.
	 */
	public toCashAddr(network: CashAddressNetworkPrefix = CashAddressNetworkPrefix.mainnet, type: CashAddressType = CashAddressType.p2pkh): string {
		return encodeCashAddress(network, type, this._hash);
	}

	/**
	 * Converts this address to its raw Hash160 as hex.
	 *
	 * @returns {string} The Hash160 of this address as hexadecimal.
	 */
	public toHash160(): string {
		return binToHex(this._hash)
	}

	public getNetwork(): string {
		return this._network
	}

	public getType(): string {
		return this._type
	}

	public static decodeAddress(address: string): DecodedAddress {
		// CashAddr w/ prefix
		const cashAddr = decodeCashAddress(address)
		if (typeof cashAddr === 'object') {
			return {
				network: cashAddr.prefix === 'bitcoincash' ? 'main' : cashAddr.prefix === 'bch-test' ? 'testnet' : 'regtest',
				hash: cashAddr.hash,
				format: 'cashaddr',
				type: cashAddr.type === CashAddressType.p2pkh ? 'p2pkh' : 'p2sh'
			}
		}

		// CashAddr w/o prefix
		const cashAddrNoPrefix = decodeCashAddressFormatWithoutPrefix(address)
		if (typeof cashAddrNoPrefix === 'object') {
			return {
				network: cashAddrNoPrefix.prefix === 'bitcoincash' ? 'main' : cashAddrNoPrefix.prefix === 'bch-test' ? 'testnet' : 'regtest',
				hash: cashAddrNoPrefix.hash,
				format: 'cashaddr',
				type: cashAddrNoPrefix.version === CashAddressVersionByte.p2pkh ? 'p2pkh' : 'p2sh'
			}
		}

		// Legacy (Base58)
		const legacyAddress = decodeBase58Address(address)
		if (typeof legacyAddress === 'object') {
			let type: string = 'unknown'
			let network: string = 'unknown'

			switch (legacyAddress.version) {
				case Base58AddressFormatVersion.p2pkh:
					type = 'p2pkh'
					network = 'main'
					break;
				case Base58AddressFormatVersion.p2sh20:
					type = 'p2sh'
					network = 'main'
					break;
				case Base58AddressFormatVersion.p2pkhTestnet:
					type = 'p2pkh'
					network = 'testnet'
					break;
				case Base58AddressFormatVersion.p2sh20Testnet:
					type = 'p2sh'
					network = 'testnet'
					break;
			}

			return {
				network,
				hash: legacyAddress.payload,
				format: 'legacy',
				type
			}
		}

		throw new Error('Could not decode address')
	}

	public static isLegacyAddress(address: string): boolean {
		return Address.decodeAddress(address).format === 'legacy'
	}

	public static isCashAddr(address: string): boolean {
		return Address.decodeAddress(address).format === 'cashaddr'
	}

	public static isHash160(address: string): boolean {
		return true
	}

	public static isMainnetAddress(address: string): boolean {
		return Address.decodeAddress(address).network === 'main'
	}

	public static isTestnetAddress(address: string): boolean {
		return Address.decodeAddress(address).network === 'testnet'
	}

	public static isRegTestAddress(address: string): boolean {
		return Address.decodeAddress(address).format === 'regtest'
	}

	public static isP2PKHAddress(address: string): boolean {
		return Address.decodeAddress(address).type === 'p2pkh'
	}

	public static isP2SHAddress(address: string): boolean {
		return Address.decodeAddress(address).type === 'p2sh'
	}

	public static detectAddressFormat(address: string): string {
		return Address.decodeAddress(address).format
	}

	public static detectAddressNetwork(address: string): string {
		return Address.decodeAddress(address).network
	}

	public static detectAddressType(address: string): string {
		return Address.decodeAddress(address).type
	}

	/**
	 * Attempt to decode an address entity from the given string.
	 *
	 * @param address {string} The address to attempt to decode.
	 *
	 * @returns {Address} The address in Legacy Address format.
	 */
	public static fromString(address: string): Address {
		const decoded = Address.decodeAddress(address)

		return new Address(decoded.hash, decoded.network)
	}

	/*public static fromXPub(xpub: string, path: string = "0/0"): Address {
		return new Address('todo') // TODO
	}

	public static fromOutputScript(scriptPubKey: string, network: string = "mainnet"): Address {
		return new Address('todo')
	}*/
}
