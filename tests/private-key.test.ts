import test from 'ava';

import { PrivateKey, HDPrivateNode, Script, Transaction, UnlockP2PKH } from '../lib/index.js';

import { binToHex, hexToBin, CashAddressNetworkPrefix, Input } from '@bitauth/libauth';

// Example success test.
test('Should generate random Private Key', t => {
	const privateKey = PrivateKey.generateRandom();

	console.log(privateKey.toWif());

	const address = privateKey.derivePublicKey().deriveAddress();

	console.log(`${address}`);

	t.pass();
});

test('Should generate XPriv', t => {
	const hdNode = HDPrivateNode.generateRandom();

	console.log(hdNode.toXPriv());
	console.log(hdNode.deriveHDPublicNode().toXPub());
	console.log(hdNode.privateKey().toWif());

	const hdNode2 = hdNode.derivePath(`m/44'/145'/0'/0/1`);

	console.log(hdNode2.toXPriv());
	console.log(hdNode2.deriveHDPublicNode().toXPub());
	console.log(hdNode2.privateKey().toWif());

	t.pass();
});

test('Should something Locking Script', t => {
	const script = Script.fromCashAddr('bitcoincash:qqzsmwalzzq70gpz20l6vct6wuytq3d2gvggzmke7c');

	console.log(script.toUint8Array());

	console.log(script.toASM());

	const script2 = Script.fromASM(script.toASM());

	console.log(script2.toUint8Array());

	console.log(script2.toASM());

	t.pass();
});

test('Should decode transaction', t => {
	const transaction = Transaction.fromHex('010000000263f7fbdebd2ccdf564208ad400515dbcf8e35c26e3ea3fae0b19967cf4a8d8a001000000644174e6050b7d5d59269ef0d43000214d6481acc31e97b14b745fda31305808ae78e6bf3bdf0610f613049106bd4904d3b31595f9d7c95ce78ce90c43597040e3ac4121031265e1eea78ec3b79a4559f68cdeba2343471b8eb8b5942b36ec7e1649cf11b0feffffffea9dd08700a67defbe21ac7287c11d99a1d63b300def4656ea5dc8e03b936ef001000000644135f6650d9fa479b4e35b65b14f13fe71236ac898d94ac9ba53c07bbefcd6e04221d736849acfde286b6ebe15c52459811b484691f9ba9bcb4066666457116d3f4121031265e1eea78ec3b79a4559f68cdeba2343471b8eb8b5942b36ec7e1649cf11b0feffffff0240420f00000000001976a914cc4813f9ffb0ee1ebb2d1f40c16e36de2248c03788ac30791000000000001976a914f979b111abbcb464c3c26dbac70b38a201e85c1688acfc470b00');

	console.log(transaction);

	t.pass();
});

test('Should build transaction', t => {
	const input = {
		outpointIndex: 0,
		outpointTransactionHash: hexToBin('4002e9c152928ed41978b7336e7cef1913737af61a2a795bbd7e255e81944388'),
		sequenceNumber: 0,
		unlockingBytecode: new Uint8Array()
	} as Input;

	const transaction = new Transaction()
		.addOutput('bitcoincash:qzcma5mn3phpf6gh5v9txa3ll3amwyrdfyqucrl9eg', 204_515n - 219n)
		.addInput(input, new UnlockP2PKH(PrivateKey.fromWIF('KwUAnfKsCgsjdMZsY66KPACM93kgmR3wwsxG5E6pHVybXsvZGyuj'), 204_515))
		.build();

	console.log(binToHex(transaction));

	const decoded = Transaction.fromBytes(transaction);

	console.log(decoded);

	const scrippy = Script.fromHex('41f1e30299aeb2be123bfd981f97630663507e0b08608aee8f9277672d50d76b28c4b71564ab309a545063a8e6563917722b0bd8140f9763e96638b233baafca6b412102d063baa21c3c2d67ae7c6ef5fd8891b27b992926548fc0c46cee91bf8a91f3f0');

	console.log(scrippy.toASM());

	console.log(Script.fromHex('416c8e9bf28654fe48a5170a83dc44558f583265f748e34b13204506af394bcb81c65959d5021b417f9bc35a8c68eef51d92e3e1173a31ec452f82cd853db4bb75412103eb1fde13223e94f46e7b7f870d35f78d68b63952b7af65fc0104b97902dfedb1').toASM());

	t.pass();
});
